var app = angular.module('myApp', ['ngRoute'])
  .config(function($routeProvider) {
    // Rutas AngularJS
    $routeProvider
      .when('/', {
        templateUrl: 'templates/index.html',
        controller: 'indexController'
      })

      .when('/login', {
        templateUrl: 'templates/login.html',
        controller: 'loginController'
      })

      .when('/tabla', {
        templateUrl: 'templates/table.html',
        controller: 'tablaController'
      })
  })
