app.controller('tablaController', function($scope, showService) {
  
  // Lista de objetos con personas.
  $scope.personas = [
    { nombre: "Juan Gamonal", edad: 23 },
    { nombre: "Brayan Ortiz", edad: 25 },
    { nombre: "Oscar Gallardo", edad: 23 }
  ];

  // Agregar nueva persona.
  $scope.newPerson = function() {
    var temp = { nombre: $scope._nombre, edad: $scope._edad };
    $scope.personas.push(temp);
    $scope._nombre = "";
    $scope._edad = "";
  }

  // Elimina persona de la tabla.
  $scope.deletePerson = function(index) {
    $scope.personas.splice(index, 1);
  }

  // Comprueba si se muestra la ayuda.
  $scope.helpFlag = false;

  // Cambia el estado de help.
  $scope.toggleHelp = function() {
    if($scope.helpFlag === false) {
      $scope.helpFlag = true;
    } else {
      $scope.helpFlag = false;
    }
  }

});
